class AddStartDateAndEndDateToProblem < ActiveRecord::Migration
  def change
    add_column :problems, :start_date, :date
    add_column :problems, :end_date, :date
  end
end
