# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  def active_class(link_path)
    current_page?(link_path) ? "active" : ""
  end
  def user
    return User.find(session[:user_id])
  end
  def user_header
    menu_items = ''
    user = User.find(session[:user_id])

    # main page
    menu_items << "<li ><p>"
    append_to menu_items, "#{I18n.t 'menu.main'}", 'main', 'list'
    menu_items << "</p></li>"
    menu_items << "<li ><p>"
    append_to menu_items, "Score", 'score', 'search'
    menu_items << "</p></li>"
    menu_items << "<li ><p>"
    append_to menu_items, "#{I18n.t 'menu.messages'}", 'messages', 'list'
    menu_items << "</p></li>"

    if (user!=nil) and (GraderConfiguration.show_tasks_to?(user))
      menu_items << "<li ><p>"
      append_to menu_items, "#{I18n.t 'menu.tasks'}", 'tasks', 'list'
      menu_items << "</p></li>"
      menu_items << "<li ><p>"
      append_to menu_items, "#{I18n.t 'menu.submissions'}", 'main', 'submission'
      menu_items << "</p></li>"
      menu_items << "<li ><p>"
      append_to menu_items, "#{I18n.t 'menu.test'}", 'test', 'index'
      menu_items << "</p></li>"
      
    end

    if GraderConfiguration['right.user_hall_of_fame']
      menu_items << "<li ><p>"
      append_to menu_items, "#{I18n.t 'menu.hall_of_fame'}", 'report', 'problem_hof'
      menu_items << "</p></li>"
    end
    menu_items << "<li ><p>"
    append_to menu_items, "#{I18n.t 'menu.help'}", 'main', 'help'
    menu_items << "</p></li>"

    if GraderConfiguration['system.user_setting_enabled']
      menu_items << "<li ><p>"
      append_to menu_items, "#{I18n.t 'menu.settings'}", 'users', 'index'
      menu_items << "</p></li>"
    end
    menu_items.html_safe
  end

  def admin_header
    menu_items = ''
    user = User.find(session[:user_id])

    if (user!=nil) and (session[:admin]) 
      # admin menu
      menu_items << "<li ><p>"
      append_to menu_items, 'Announcements', 'announcements', 'index'
      menu_items << "</p></li>"
      menu_items << "<li ><p>"
      append_to menu_items, 'Msg console', 'messages', 'console'
      menu_items << "</p></li>"
      menu_items << "<li ><p>"
      append_to menu_items, 'Problems', 'problems', 'index'
      menu_items << "</p></li>"
       menu_items << "<li ><p>"
      append_to menu_items, 'Chart', 'chart', 'index'
      menu_items << "</p></li>"
      menu_items << "<li ><p>"
      append_to menu_items, 'Users', 'user_admin', 'index'
      menu_items << "</p></li>"
      menu_items << "<li ><p>"
      append_to menu_items, 'Results', 'user_admin', 'user_stat'
      menu_items << "</p></li>"
      menu_items << "<li ><p>"
      append_to menu_items, 'Plagiarism', 'plagiarism', 'index'
      menu_items << "</p></li>"
      menu_items << "<li ><p>"
      append_to menu_items, 'Report', 'report', 'login_stat'
      menu_items << "</p></li>"
      menu_items << "<li ><p>"
      append_to menu_items, 'Graders', 'graders', 'list'
      menu_items << "</p></li>"
      menu_items << "<li ><p>"
      append_to menu_items, 'Contests', 'contest_management', 'index'
      menu_items << "</p></li>"
      menu_items << "<li ><p>"
      append_to menu_items, 'Sites', 'sites', 'index'
      menu_items << "</p></li>"
      menu_items << "<li ><p>"
      append_to menu_items, 'System config', 'configurations', 'index'
      menu_items << "</p></li>"
    end

    menu_items.html_safe
  end

  def append_to(option,label, controller, action)
    option << ' ' if option!=''
    option << link_to_unless_current(label,
                                     :controller => controller,
                                     :action => action)
  end

  def format_short_time(time)
    now = Time.now.gmtime
    st = ''
    if (time.yday != now.yday) or
	(time.year != now.year)
      st = time.strftime("%x ")
    end
    st + time.strftime("%X")
  end

  def format_short_duration(duration)
    return '' if duration==nil
    d = duration.to_f
    return Time.at(d).gmtime.strftime("%X")
  end

  def read_textfile(fname,max_size=2048)
    begin
      File.open(fname).read(max_size)
    rescue
      nil
    end
  end

  def user_title_bar(user)
    header = ''
    time_left = ''

    #
    # if the contest is over
    if GraderConfiguration.time_limit_mode?
      if user.contest_finished?
        header = <<CONTEST_OVER
<tr><td colspan="2" align="center">
<span class="contest-over-msg">THE CONTEST IS OVER</span>
</td></tr>
CONTEST_OVER
      end
      if !user.contest_started?
        time_left = "&nbsp;&nbsp;" + (t 'title_bar.contest_not_started')
      else
        time_left = "&nbsp;&nbsp;" + (t 'title_bar.remaining_time') + 
          " #{format_short_duration(user.contest_time_left)}"
      end
    end
    
    #
    # if the contest is in the anaysis mode
    if GraderConfiguration.analysis_mode?
      header = <<ANALYSISMODE
<tr><td colspan="2" align="center">
<span class="contest-over-msg">ANALYSIS MODE</span>
</td></tr>
ANALYSISMODE
    end

    contest_name = GraderConfiguration['contest.name']

    #
    # build real title bar
    result = <<TITLEBAR
<div class="title">
<table>
#{header}
<tr>
<td class="left-col">
#{user.full_name}<br/>
#{t 'title_bar.current_time'} #{format_short_time(Time.zone.now)}
#{time_left}
<br/>
</td>
<td class="right-col">#{contest_name}</td>
</tr>
</table>
</div>
TITLEBAR
    result.html_safe
  end

  def markdown(text)
    markdown = RDiscount.new(text)
    markdown.to_html.html_safe
  end

end
