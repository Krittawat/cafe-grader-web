require "moss_ruby"

class PlagiarismController < ApplicationController
  before_filter :admin_authorization
  def check
    # Create the MossRuby object
    moss = MossRuby.new(377725571)
    
    # Set options  -- the options will already have these default values
    moss.options[:max_matches] = 10
    moss.options[:directory_submission] =  false
    moss.options[:show_num_matches] = 250
    moss.options[:experimental_server] =    false
    moss.options[:comment] = ""
    language = {"cpp"=>"cc", "c"=>"c", "py"=>"python", "java"=>"java", "rb"=>"python"}
    # Create a file hash, with the files to be processed
    to_check = MossRuby.empty_file_hash

    users = ((Dir.entries("/home/cafe/cafe_grader/judge/result/")).sort)
    users = users[2,users.size]
    @problem = Problem.find_by_id(params[:problem][:id])
    users.each{ |user|
      begin
        file = (Dir.entries("/home/cafe/cafe_grader/judge/result/#{user}/#{@problem[:name]}")).sort
        file_search = Dir.glob("/home/cafe/cafe_grader/judge/result/#{user}/#{@problem[:name]}/#{file[-1]}/*.*")
        if Dir.glob(file_search).length == 0
				  next
			  end
        MossRuby.add_file(to_check, "/home/cafe/cafe_grader/judge/result/#{user}/#{@problem[:name]}/#{file[-1]}/*.*")
      rescue
        next
      end
  }

    # Get server to process files
    
    @url = moss.check to_check

    # Get results
    @results = moss.extract_results @url
    
    # Use results
    
    # puts "Got results from #{@url}"
    count_match = -1
    # @results.each { |match|
    #     count_match += 1
    #     puts "----"
    #     match.each { |file|
    #         puts "#{file[:filename]} #{file[:pct]} #{@url}/match#{count_match}.html"
    #     }
    # }

    render :action => 'index'
  end

  def get_problems
    problems = Problem.find_problems_from_contest_id(params[:id])
    render json:problems
  end
end
