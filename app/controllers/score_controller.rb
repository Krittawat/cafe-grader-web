class ScoreController < ApplicationController
    before_filter :authenticate

    def search
		@user = User.find(session[:user_id])
		puts "-------------------------------------"
		puts (params)
		if params[:contest] != nil and params[:contest][:id] != ''	
			@contest = Contest.find(params[:contest][:id])
			@problems = Problem.find_problems_from_contest_id(params[:contest][:id])
		
			@scorearray = Array.new
				
			@problems.each do |p|
				ustat = Array.new
				ustat[0] = p.name
				sub = Submission.find_last_by_user_and_problem(@user.id,p.id)
				if (sub!=nil) and (sub.points!=nil) 
					ustat << [(sub.points.to_f*100/p.full_score).round, (sub.points>=p.full_score)]
				else
					ustat << [0,false]
				end
				@scorearray << ustat
			end
			puts @user.contests.collect {|c| c.name}
		end
    end
end
