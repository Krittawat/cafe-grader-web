class ChartController < ApplicationController
    def get_data
      if params[:max_average] != nil
            @param = 'max'
      end
	    @problems = Problem.find_problems_from_contest_id(params[:contest][:id])
      @users = User.find_users_from_contest_id(params[:contest][:id])
	   	@contest_id = params[:contest][:id]
	    @scorearray = Array.new
	    @users.each do |u|
	      ustat = Array.new
	      ustat[0] = u
	      @problems.each do |p|
	        sub = Submission.find_last_by_user_and_problem(u.id,p.id)
	        if (sub!=nil) and (sub.points!=nil) 
	          ustat << [(sub.points.to_f*100/p.full_score).round, (sub.points>=p.full_score)]
	        else
	          ustat << [0,false]
	        end
	      end
	      @scorearray << ustat
	    end
			puts @scorearray
	    render template: 'chart/index'
  	end
end

